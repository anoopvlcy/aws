{
  "name": "Amazon Web Services",
  "description": "Deploy your application to [Amazon Web Services](https://aws.amazon.com/)",
  "template_path": "templates/gitlab-ci-aws.yml",
  "kind": "hosting",
  "prefix": "aws",
  "is_component": true,
  "variables": [
    {
      "name": "AWS_CLI_IMAGE",
      "description": "The Docker image used to run AWS CLI commands",
      "default": "registry.hub.docker.com/amazon/aws-cli:latest"
    },
    {
      "name": "AWS_BASE_APP_NAME",
      "description": "Base application name",
      "default": "$CI_PROJECT_NAME",
      "advanced": true
    },
    {
      "name": "AWS_ENVIRONMENT_URL",
      "type": "url",
      "description": "The default environments url _(only define for static environment URLs declaration)_\n\n_supports late variable expansion (ex: `https://%{environment_name}.aws.acme.com`)_"
    },
    {
      "name": "AWS_SCRIPTS_DIR",
      "description": "Directory where AWS scripts (deploy & cleanup) are located",
      "default": ".",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "review",
      "name": "Review",
      "description": "Dynamic review environments for your topic branches (see GitLab [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/))",
      "enable_with": "AWS_REVIEW_ENABLED",
      "variables": [
        {
          "name": "AWS_REVIEW_APP_NAME",
          "description": "The application name for review env (only define to override default)",
          "advanced": true
        },
        {
          "name": "AWS_REVIEW_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop `review` environments",
          "default": "4 hours"
        },
        {
          "name": "AWS_REVIEW_ENVIRONMENT_URL",
          "type": "url",
          "description": "The review environments url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        }
      ]
    },
    {
      "id": "integration",
      "name": "Integration",
      "description": "A continuous-integration environment associated to your integration branch (`develop` by default)",
      "enable_with": "AWS_INTEG_ENABLED",
      "variables": [
        {
          "name": "AWS_INTEG_APP_NAME",
          "description": "The application name for integration env (only define to override default)",
          "advanced": true
        },
        {
          "name": "AWS_INTEG_ENVIRONMENT_URL",
          "type": "url",
          "description": "The integration environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        }
      ]
    },
    {
      "id": "staging",
      "name": "Staging",
      "description": "An iso-prod environment meant for testing and validation purpose on your production branch (`main` or `master` by default)",
      "enable_with": "AWS_STAGING_ENABLED",
      "variables": [
        {
          "name": "AWS_STAGING_APP_NAME",
          "description": "The application name for staging env (only define to override default)",
          "advanced": true
        },
        {
          "name": "AWS_STAGING_ENVIRONMENT_URL",
          "type": "url",
          "description": "The staging environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        }
      ]
    },
    {
      "id": "prod",
      "name": "Production",
      "description": "The production environment",
      "enable_with": "AWS_PROD_ENABLED",
      "variables": [
        {
          "name": "AWS_PROD_APP_NAME",
          "description": "The application name for production env (only define to override default)",
          "advanced": true
        },
        {
          "name": "AWS_PROD_ENVIRONMENT_URL",
          "type": "url",
          "description": "The production environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "AWS_PROD_DEPLOY_STRATEGY",
          "description": "Defines the deployment to production strategy.",
          "type": "enum",
          "values": ["manual", "auto"],
          "default": "manual"
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "oidc",
      "name": "OpenID Connect",
      "description": "Enables [federated authentication using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/)",
      "template_path": "templates/gitlab-ci-aws-oidc.yml",
      "variables": [
        {
          "name": "AWS_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "AWS_OIDC_ROLE_ARN",
          "description": "Default IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/)",
          "advanced": true
        },
        {
          "name": "AWS_REVIEW_OIDC_ROLE_ARN",
          "description": "IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `review` env _(only define to override default)_",
          "advanced": true
        },
        {
          "name": "AWS_INTEG_OIDC_ROLE_ARN",
          "description": "IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `integration` env _(only define to override default)_",
          "advanced": true
        },
        {
          "name": "AWS_STAGING_OIDC_ROLE_ARN",
          "description": "IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `staging` env _(only define to override default)_",
          "advanced": true
        },
        {
          "name": "AWS_PROD_OIDC_ROLE_ARN",
          "description": "IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `production` env _(only define to override default)_",
          "advanced": true
        }
      ]
    },
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-aws-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url",
          "mandatory": true
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
